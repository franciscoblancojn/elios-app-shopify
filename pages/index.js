import { Spinner, Page } from "@shopify/polaris";
import { useState, useEffect } from "react";
import getConfig from 'next/config'

const Loader = () => {
    return (
        <div className="loader">
			    <Spinner
            accessibilityLabel="Loading form field"
            hasFocusableParent={false}
          />
        </div>
    )
}
const API = () => {
  const URL = "https://pixeltracking.startscoinc.com/app-shopify/api"
  const URLPIXEL = "https://pixeltracking.startscoinc.com/appM"
  const request = async (json,url) => {
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");

    var raw = JSON.stringify(json);

    var requestOptions = {
        method: 'POST',
        headers: myHeaders,
        body: raw,
        redirect: 'follow'
    };
    try {
      const response = await fetch(url, requestOptions)
      const result = await response.json()
      return result
    } catch (error) {
      return {
        type:"error",
        error
      }
    }
  }
  const saveKey = async (key,token,shop) => {
    return await request({
        type:"saveKey",
        token,
        shop,
        key,
        icon: "",
        cms:"shopify"
    },URL)
  }
  const getToken = async (APIKEY,shop) => {
    return await request({
      "type":"getTokenShopify",
      "mongoCheckVerify":APIKEY,
      "query":{
          "host": shop
      }
    },URLPIXEL)
  }
  const getKey = async (_id) => {
    return await request({
      "type":"getKeyBy_ID",
      _id
    },URLPIXEL)
  }
  return {
    saveKey,
    getToken,
    getKey
  }
}
const APIELIOS = API()

const Index = ({query}) => {
  const [load, setLoad] = useState(false)
  const [config, setConfig] = useState({
    key:"",
    shop:"",
    URLAPI:"",
    APIKEY:"",
    token:"",
    _id:"",
    result:{
      type:"",
      msj:""
    }
  })

  const handelChange = (e) => {
    const id = e.target.id
    const value = e.target.value
    setConfig({
      ...config,
      [id]:value
    })
  }
  const onSave = async () => {
    setConfig({
      ...config,
      result:{
        type:"",
        msj:""
      }
    })
    const result = await APIELIOS.saveKey(config.key,config.token,config.shop)
    setConfig({
      ...config,
      result
    })
  
  }
  const loadConfig = async () => {
		const {publicRuntimeConfig} = getConfig()
		const {APIKEY,URLAPI} = publicRuntimeConfig
		const {shop} = query
    if(shop && APIKEY){
      var token = await APIELIOS.getToken(APIKEY,shop)
      if(token.type == "error"){
        setConfig({
          ...config,
          result:{
            type:"error",
            msj:"Ocurrio un error"
          }
        })
        return
      }
      token = token[0]
      var _id = ""
      var key = ""
      if(token){
        _id = token._id
        token = token.token
        key = await APIELIOS.getKey(_id)
        if(key){
          if(key.type == "error"){
            setConfig({
              ...config,
              result:{
                type:"error",
                msj:"Ocurrio un error"
              }
            })
            return
          }
          key = key[0]
          if(!key){
            key = ""
          }else{
            key = key.key
          }
        }
        if(!key){
          key = ""
        }
      }else{
        token = ""
      }

      setConfig({
        ...config,
        shop,
        URLAPI,
        APIKEY,
        token,
        _id,
        key
      })
      setLoad(true)
		}
  }

  useEffect(() => {
    loadConfig()
  }, [])
  
  return (
  <Page>
    {
      !load ?
      <Loader/>
      :
      <div>
        <div className="content-img">
          <img src="https://app.eliosanalytics.com/img/elios.png" />
          <h1 className="eliosTitle">elios</h1>
        </div>
        <input type="password" name="key" id="key" className="inputForm" placeholder="key" value={config.key} onChange={handelChange}/>
        <p id="msj" className={config.result.type}>{config.result.msj}</p>
        <button id="save" className="btnForm" onClick={onSave}>Save</button>
      </div>
    }
    <style jsx global>
        {`
          @import url('https://fonts.googleapis.com/css2?family=Open+Sans&display=swap');
          :root{
            --header-height:60px;

            --color-1   : #fff;
            --color-2   : #DADADA;
            --color-3   : #B5DCFF;
            --color-4   : #A7B5FF;
            --color-5   : #5F78FE;
            --color-6   : #929292;
            --color-7   : #878787;
            --color-8   : #606060;
            --color-9   : #646464;
            --color-10  : #4E4E4E;
            --color-11  : #EFEFEF;
            --color-12  : #B5C1FF;
            --color-13  : #EFF2FF;
            --color-14  : #DFDFDF;

            --color-e-1 : rgb(0 0 0 / 20%);
            --color-e-2 : hsl(0deg 0% 0% / 16%);

            --font-size :11px;

            --z-index-1 : 10;
            --z-index-2 : 50;
            --z-index-3 : 100;

            --translateMenu:0%;
            --transition-menuH:.5s;
          }
          body{
            display:block !important;
          }
          .content-img{
              max-width: 400px;
              margin: auto;
              text-align: center;
          }
          .loader {
            position: fixed;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            margin: auto;
            width: 100%;
            height: 100%;
            z-index: 351;
            display: flex;
            align-items: center;
            justify-content: center;
            background: #0000001a;
          }
          .inputForm,
          .btnForm{
              width: 453px;
              height: 78px;
              max-width: 100%;
              background-color: var(--color-11);
              color: var(--color-7);
              margin-bottom: 27px;
              font-size: 28px;
              font-weight: bold;
              padding: 0 32px;
              display: flex;
              align-items: center;
              line-height: 1;
              border:0;
              border-radius: 20px;
              outline: none;
              text-decoration: none;
              margin: 50px auto;
          }
          .btnForm{
              cursor: pointer;
              color: var(--color-1);
              justify-content: center;
              background: linear-gradient(to right,var(--color-3) 0%,var(--color-4) 100%);
          }
          .eliosTitle{
            font-size: 72px;
            line-height: 1;
            text-align:center;
            background: linear-gradient(to right,var(--color-3) 0%,var(--color-4) 100%);
            -webkit-background-clip: text;
            -webkit-text-fill-color: transparent;
            font-family: 'Open Sans ', sans-serif;
            font-weight: 700;
          }
          img{
            max-width: 100%;
          }
          #msj{
              font-size: 20px;
              text-align:center;
          }
          .error{
              color: red;
          }
          .warning{
              color: orange;
          }
          .ok{
              color: green;
          }
        
        `}
    </style>
  </Page>
  )
}

export async function getServerSideProps(context) {
	const { query } = context;
	return {
		props:{ query }
	}
}
export default Index;
